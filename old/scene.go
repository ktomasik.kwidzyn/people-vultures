package main

import (
  "log"
  "github.com/gen2brain/raylib-go/raylib"
)

type Scene struct {
  Name string
  BackgroundId string
}

func NewScene(name, backgroundId string) Scene {
  return Scene{name, backgroundId}
}

func NewEmptyScene() Scene {
  return Scene{"", ""}
}

type CurrentScene struct {
  Name string
  Background rl.Texture2D
}

func (g Game) SetCurrentSceneByName(sceneName string) {
  if g.CurrentScene.Name == sceneName {
    log.Print("[GAME][WARNING]: Tried to open scene with same name as current. Ignoring.")
    return
  }

  log.Print("[TEST]:-", sceneName)
  for _, scene := range g.Scenes {
    log.Print("[TEST]: ", scene.Name)
    if scene.Name == sceneName {
      background, err := g.GetTextureByName(scene.BackgroundId)
      if err != nil {
        log.Print(err)
        return
      }
      g.CurrentScene = CurrentScene{sceneName, background}
      return
    }
  }
  log.Print("[GAME][WARNING]: Tried to open scene with invalid name. Ignoring.")
}
