package main

import (
  "bufio"
  "fmt"
  "os"
  "strings"
)

func main() {

  fi, err := os.Open("TEMPLATE.txt")
  if err != nil {
      panic(err)
  }
  defer func() {
    if err := fi.Close(); err != nil {
        panic(err)
    }
  }()

  s := bufio.NewScanner(bufio.NewReader(fi))

  for s.Scan() {
    line := s.Text()
    if !strings.HasPrefix(line, "//") {
      declaration := strings.Split(line, " ")
      //Implement interpretation of scene txt file commands.
      if declaration[0] == "scene" {
        fmt.Printf("Scene name: %s\n", declaration[1])
      } else if declaration[0] == "background" { 
        fmt.Printf("Scene background path: %s\n", declaration[1])
      }
    }
	}

}
