package engine

type Movement interface {
  X() float32
  Y() float32
}
