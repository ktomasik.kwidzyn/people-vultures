# People Vultures

A simple(as for now) beat'em up platformer game made with made with [raylib](https://www.raylib.com/). Written in Go.

The main character has to be small, jumpy person with knife. Combat mechanics will include sick combos designed to keep enemy in air(Dante, oh Dante!), and an error-forgiving but action-packed fights where the player has to evade most of the attacks due to the 2hits=death rule.

SHORT-TERM TASK LIST:
Current goal: The game is playable.
* [ ] Created a **working** 2D physics engine.
* [ ] The player character exists and is controllable.
* [ ] First scene – with informations read from file.

LONG-TERM GOAL LIST:
* [ ] The game is playable.
* [ ] Nicely tuned character controls.
* [ ] Progress saving.
* [ ] Combo chains.
* [ ] Bossfights.
* [ ] Graphics.
* [ ] Scenes and transitions.
* [ ] Sound Effects.
* [ ] Soundtrack.
* [ ] User Interface.
