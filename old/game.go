package main

import (
  "bufio"
  "errors"
  "io/ioutil"
  "log"
  "os"
  "strings"
  "github.com/gen2brain/raylib-go/raylib"
)

//Path is stored in the struct to avoid double loading a texture during game loading.
type Sprite struct {
  Name string
  Texture rl.Texture2D
}

type Game struct {
  Paused bool
  Textures []Sprite
  Scenes []*Scene
  CurrentScene CurrentScene
}

func NewGame() Game {
  g := Game{false, []Sprite{}, []*Scene{}, CurrentScene{"", rl.Texture2D{}}} //The CurrentScene parameter will be overwritten at init anyway.
  return g
}

func (g Game) Init() {
  g.LoadScenes()
  g.LoadImages()
  g.SetCurrentSceneByName("name")

	for !rl.WindowShouldClose() {
    g.Draw()
  }
}

func (g Game) Draw() {
  rl.BeginDrawing()
  rl.ClearBackground(rl.RayWhite)
  rl.DrawTexture(g.CurrentScene.Background, 0, 0, rl.Black)
  //Draw here

  rl.EndDrawing()
}

func (g Game) LoadImages() {
  log.Print("[LOADING]: Scanning assets directory")
  files, err := ioutil.ReadDir("./assets")
  if err != nil {
    log.Fatal(err)
  }

  for _, asset := range files {
    if !asset.IsDir() && strings.HasSuffix(asset.Name(), ".png"){
      g.Textures = append(g.Textures, Sprite{asset.Name(), rl.LoadTexture("./assets/"+asset.Name())})
    }
  }
}

func (g Game) GetTextureByName(name string) (rl.Texture2D, error) {
  for _, texture := range g.Textures {
    if texture.Name == name {
      return texture.Texture, nil
    }
  }
  return g.Textures[0].Texture, errors.New("[GAME][ERROR][NONFATAL]: Tried to load invalid texture name. Defaulting to first texture in the memory.")
}

func (g Game) LoadScenes() {
  log.Print("[LOADING]: Scanning scenes directory")
  files, err := ioutil.ReadDir("./scenes")
  if err != nil {
    log.Fatal(err)
  }

  for _, scene := range files {
    if !scene.IsDir() && strings.HasSuffix(scene.Name(), ".txt"){
      g.LoadSceneFromTxt("./scenes/"+scene.Name())
      log.Print(len(g.Scenes))
    }
  }
  log.Print(len(g.Scenes))
  //TODO: figure out why the fuck the LoadSceneFromTxt appends the scene and then it is gone.
}

func (g Game) LoadSceneFromTxt(path string) {
  fi, err := os.Open(path)
  if err != nil {
      log.Fatal(err)
  }
  defer func() {
    if err := fi.Close(); err != nil {
        panic(err)
    }
  }()

  s := bufio.NewScanner(bufio.NewReader(fi))
  scene := NewEmptyScene()

  for s.Scan() {
    line := s.Text()
    if !strings.HasPrefix(line, "//") {
      declaration := strings.Split(line, " ")
      //Implement interpretation of scene txt file commands.
      if declaration[0] == "scene" {
        if declaration[1] == "" {
          log.Fatal("[LOADING][SCENE]: Invalid scene name, in ", path)
        }
        scene.Name = declaration[1]
      } else if declaration[0] == "background" {
        if declaration[1] == "" {
          log.Fatal("[LOADING][SCENE]: Invalid scene background in ", path)
        }
        scene.BackgroundId = declaration[1]
      }
    }
	}

  if scene.Name == "" || scene.BackgroundId == "" {
    log.Fatal("[LOADING][SCENE]: Scene recieved invalid parameters in ", path)
  }

  log.Print("[LOADING]: Scene Added")
  g.Scenes = append(g.Scenes, &scene)
  log.Print(len(g.Scenes))

}
