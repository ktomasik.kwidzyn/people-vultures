package main

import rl "github.com/gen2brain/raylib-go/raylib"
import game "./game"

func main() {
	g := game.InitializeGame()
	g.StartLoop()
  rl.CloseWindow()
}
