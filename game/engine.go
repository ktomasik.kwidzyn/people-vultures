package game

import rl "github.com/gen2brain/raylib-go/raylib"
import player "./player"
import interfaces "./interfaces"
import "fmt"

type Game struct {
  Current *Scene
  Player *player.Player
}

func (g *Game) StartLoop() {
  for !rl.WindowShouldClose() {
    g.Player.CurrentAction.LoopAction(g.Player)
    g.MoveEntity(g.Player)
    g.Display()
	}
}

func (g *Game) Display() {
  rl.BeginDrawing()

  rl.ClearBackground(rl.RayWhite)

  rl.DrawText(fmt.Sprintf("Player X position: %d", g.Player.X), 190, 200, 20, rl.LightGray)
  rl.DrawText(fmt.Sprintf("Player Y position: %d", g.Player.Y), 190, 220, 20, rl.LightGray)
  rl.DrawTexture(g.Player.GetTexture(), g.Player.X, g.Player.Y, rl.Black)
  g.Current.Draw()

  rl.EndDrawing()
}

func (g *Game) MoveEntity(ent interfaces.Entity){
  for _, terrain := range g.Current.Terrain {
    x, y := ent.GetMovementVectorNormal()
    rec := rl.GetCollisionRec(ent.GetMovementHitbox(), terrain.GetHitbox())

    //If entity started above or below given terrain object, clip it out during collision. Otherwise - don't.
    preMovRec := rl.GetCollisionRec(ent.GetHitbox(), terrain.GetHitbox())

    //If Entity is clipped inside the object.
    if preMovRec.Width > 0 && preMovRec.Height > 0 {
      fmt.Printf("Clip!\n")
      ent.SetMovementY(ent.GetMovementY() - (y*int32(rec.Height)))
      ent.SetMovementX(ent.GetMovementX() - (x*int32(rec.Width)))
    } else {

      //If Entity would be clipped, fix it's Y pos.
      //TODO: This still clips when moving up/down next to ledge.
      //Desired behaviour near ledges:
      // If entity is player and is in the most 20 px, the character climb-jumps onto the Terrain
      // Otherwise, do nuthin.
      //fmt.Printf("%f\n", rec.Height)
      if rec.Height > 0 && rec.Width > 0 {
        //To avoid clipping after multiple jumping, firstly check if the entity's top is below terrain's bottom.
        if ent.GetMovementY() < 0 && int32(terrain.GetHitbox().Y + terrain.GetHitbox().Height) < ent.GetY() {
          fmt.Printf("bump!")
          ent.SetMovementY(0)
          /*If entity is rising,
          and it was directly BELOW the object BEFORE the movement
          */
        }

        ledgeHeight := float32(ent.GetY())+ent.GetHitbox().Height - rec.Y
        fmt.Printf("%f\n", ledgeHeight)
        if rec.Width < 10 && ledgeHeight < 20 && ledgeHeight > 0 && ent.GetMovementY() < 4 {
          /*
          If the entity is on the ledge(
          ait's base is little below the top of given terrain
          and its potential clip is less than 10px.
          AND it's falling speed is less than 3.
          ):
          */
          ent.SetMovementY(-12)
        } else if ent.GetMovementY() > 0 && ledgeHeight <= 0 {
          ent.SetMovementY(ent.GetMovementY() - (y*int32(rec.Height)))
          //If entity is falling,
          //Has base ABOVE the top of the terrain:
        }

        //Then CHECK IF COLLISION STILL HAPPENS. IF IT IS SO, FIX X.
        rec = rl.GetCollisionRec(ent.GetMovementHitbox(), terrain.GetHitbox())
        if rec.Height > 0 && rec.Width > 0 {
          ent.SetMovementX(ent.GetMovementX() - (x*int32(rec.Width)))
        }

    }}
  ent.SetX(ent.GetX() + ent.GetMovementX())
  ent.SetY(ent.GetY() + ent.GetMovementY())
  ent.SetMovementX(0)
  ent.SetMovementY(ent.GetMovementY()+1)
  }
}

func InitializeGame() *Game {
  rl.InitWindow(800, 450, "raylib [core] example - basic window")
  rl.SetTargetFPS(60)
  g := &Game{CreateTestingScene(), player.InitializePlayer()}
  return g
}
