package game

import rl "github.com/gen2brain/raylib-go/raylib"

type TerrainObject struct {
  X, Y int32
  Texture *rl.Texture2D
  Hitbox *rl.Rectangle
}

func (t *TerrainObject) GetTexture() rl.Texture2D {
  return *t.Texture
}

func (t *TerrainObject) GetHitbox() rl.Rectangle {
  return *t.Hitbox
}

func CreateTestingTerrain() *TerrainObject {
  tex := rl.LoadTexture("assets/placeholder_chain.png")
  return &TerrainObject{256, 50, &tex, &rl.Rectangle{256, 50, 128, 64}}
}
