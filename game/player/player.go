package player

import rl "github.com/gen2brain/raylib-go/raylib"
import interfaces "../interfaces"
import actions "../actions"

type Player struct {
  X, Y int32
  MovementX, MovementY int32
  texture *rl.Texture2D
  CurrentAction interfaces.Action
  hitbox *rl.Rectangle
}

func (p *Player) GetTexture() rl.Texture2D {
  return *p.texture
}

func (p *Player) SetX(newX int32) {
  p.X = newX
}

func (p *Player) SetY(newY int32) {
  p.Y = newY
}

func (p *Player) GetX() int32 {
  return p.X
}

func (p *Player) GetY() int32 {
  return p.Y
}

func (p *Player) SetMovementX(newX int32) {
  p.MovementX = newX
}

func (p *Player) SetMovementY(newY int32) {
  p.MovementY = newY
}

func (p *Player) GetMovementX() int32 {
  return p.MovementX
}

func (p *Player) GetMovementY() int32 {
  return p.MovementY
}

func (p *Player) GetMovementVectorNormal() (int32, int32) {
  var x, y int32 = 0, 0
  if p.MovementX < 0 {
    x = -1
  } else if p.MovementX > 0  {
    x = 1
  }
  if p.MovementY < 0 {
    y = -1
  } else if p.MovementY > 0 {
    y = 1
  }
  return x, y
}

func (p *Player) GetMovementHitbox() rl.Rectangle {
  return rl.Rectangle{float32(p.X+p.MovementX)+p.hitbox.X, float32(p.Y+p.MovementY)+p.hitbox.Y, p.hitbox.Width, p.hitbox.Height}
}

func (p *Player) GetHitbox() rl.Rectangle {
  return rl.Rectangle{float32(p.X)+p.hitbox.X, float32(p.Y)+p.hitbox.Y, p.hitbox.Width, p.hitbox.Height}
}

func InitializePlayer() *Player {
  tex := rl.LoadTexture("./assets/placeholder.png")
  return &Player{0, 0, 0, 0, &tex, actions.PlayerActionIdle{}, &rl.Rectangle{0, 0, 64, 64}}
}
