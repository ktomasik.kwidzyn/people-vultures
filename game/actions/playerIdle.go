package game

import rl "github.com/gen2brain/raylib-go/raylib"
import interfaces "../interfaces"

//Default action - Every player action should get back to this.
type PlayerActionIdle struct {
  currentFrame int32
}

func (a PlayerActionIdle) LoopAction(ent interfaces.Entity) {
  if rl.IsKeyDown(262) {
    ent.SetMovementX(5)
  } else if rl.IsKeyDown(263) {
    ent.SetMovementX(-5)
  }
  if rl.IsKeyPressed(265) {
    ent.SetMovementY(-15)
    //Jump
  } //else if rl.IsKeyDown(264) {
  //  ent.SetMovementY(5)
  //}
  a.currentFrame++
}

func (a PlayerActionIdle) GetTextureCoords() (uint8, uint8) {
  return a.currentFrame % 2, 0
}
