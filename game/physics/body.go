package physics

import rl "github.com/gen2brain/raylib-go/raylib"

type Body interface {
  SetX(int32)
  SetY(int32)
  SetMovementX(int32)
  SetMovementY(int32)
  GetX() int32
  GetY() int32
  GetMovementX() int32
  GetMovementY() int32
  GetHitbox()
}

func GetVectorOfCollision(b1, b2 rl.Rectangle) {
  
}
