package game

import rl "github.com/gen2brain/raylib-go/raylib"

type Scene struct {
  Terrain []*TerrainObject
}

func (s *Scene) Draw() {
  for _, obj := range s.Terrain {
    rl.DrawTexture(obj.GetTexture(), obj.X, obj.Y, rl.White)
  }
}

func CreateTestingScene() *Scene {
  return &Scene{[]*TerrainObject{CreateTestingTerrain()}}
}
