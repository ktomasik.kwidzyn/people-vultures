package interfaces

import rl "github.com/gen2brain/raylib-go/raylib"

type Drawable interface {
  GetTexture() rl.Texture2D
}

type Entity interface {
  SetX(int32)
  SetY(int32)
  SetMovementX(int32)
  SetMovementY(int32)
  GetX() int32
  GetY() int32
  GetHitbox() rl.Rectangle
  GetMovementX() int32
  GetMovementY() int32
  GetMovementHitbox() rl.Rectangle
  GetMovementVectorNormal() (int32, int32)
}

type Action interface {
  LoopAction(Entity)
  GetTextureCoords() (uint8, uint8)
}
